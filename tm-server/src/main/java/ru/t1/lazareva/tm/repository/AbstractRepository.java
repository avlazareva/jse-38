package ru.t1.lazareva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.DBConstants;
import ru.t1.lazareva.tm.api.repository.IRepository;
import ru.t1.lazareva.tm.comparator.CreatedComparator;
import ru.t1.lazareva.tm.comparator.StatusComparator;
import ru.t1.lazareva.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return DBConstants.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return DBConstants.COLUMN_STATUS;
        else return DBConstants.COLUMN_NAME;
    }


    @NotNull
    public abstract M fetch(@NotNull final ResultSet row) throws Exception;

    @NotNull
    public abstract M add(@NotNull final M model) throws Exception;

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) throws Exception {
        clear();
        return add(models);
    }

    @Override
    public void clear() throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String query = String.format("SELECT * FROM %s", getTableName());
        @NotNull final String order = String.format(" ORDER BY %s", getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(query + order);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String query = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    public Collection<M> add(@NotNull Collection<M> models) throws Exception {
        @NotNull List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        @NotNull final String query = String.format("SELECT * FROM %s LIMIT ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) {
                if (!resultSet.next()) return null;
            }
            return fetch(resultSet);
        }
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String query = String.format("SELECT COUNT(ID) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) throws Exception {
        @NotNull final String query = String.format("DELETE FROM %s WHERE ID = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) throws Exception {
        @Nullable final M resultModel = findOneById(id);
        if (resultModel == null) return null;
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("delete from %s where id = ?;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) throws Exception {
        @Nullable final M resultModel = findOneByIndex(index);
        if (resultModel == null) return null;
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("delete from %s where index = ?;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            statement.executeUpdate();
        }
        return resultModel;
    }


    @Override
    public void removeAll(@Nullable final Collection<M> collection) throws Exception {
        for (M model : collection) {
            remove(model);
        }
    }

}