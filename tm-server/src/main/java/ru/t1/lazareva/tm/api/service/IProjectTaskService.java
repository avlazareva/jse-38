package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

    void removeProjectById(@Nullable String userId, @Nullable String projectId) throws Exception;

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

}