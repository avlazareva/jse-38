package ru.t1.lazareva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.model.User;

import java.sql.ResultSet;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User fetch(@NotNull ResultSet row) throws Exception;

    @NotNull
    User add(@NotNull User user) throws Exception;

    void update(@NotNull User user) throws Exception;

    @NotNull
    User create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password, @NotNull String email) throws Exception;

    @NotNull
    User create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password, @NotNull Role role) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    @NotNull
    Boolean isLoginExist(@NotNull String login) throws Exception;

    @NotNull
    Boolean isEmailExist(@NotNull String email) throws Exception;

}
