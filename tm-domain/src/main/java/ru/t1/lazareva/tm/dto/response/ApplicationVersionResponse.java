package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class ApplicationVersionResponse extends AbstractResponse {

    private String version;

}
